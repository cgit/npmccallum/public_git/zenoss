%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

%global upstrname eggtestinfo

Name:           python-%{upstrname}
Version:        0.2.2
Release:        1%{?dist}
Summary:        Python setuptools plugin for adding test information to .egg-info

Group:          Development/Languages
License:        Python Software Foundation License or Zope Public License
URL:            http://pypi.python.org/pypi/%{upstrname}
Source0:        http://pypi.python.org/packages/source/e/%{upstrname}/%{upstrname}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel
Requires:       python-setuptools

%description
This package is a setuptools plugin: it adds
a file to the generated .egg-info directory,
capturing the information used by the setup.py
test command when running tests.

%prep
%setup -q -n %{upstrname}-%{version}

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
 
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/*

%changelog
* Fri Jun 18 2010 Nathaniel McCallum - 0.2.2-1
- Initial package

