%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

Name:           pyip
Version:        0.7
Release:        2%{?dist}
Summary:        Python assembling/disassembling of raw ip packets

Group:          Development/Languages
License:        Python
URL:            http://pypi.python.org/pypi/pyip
Source0:        http://pypi.python.org/packages/source/p/%{name}/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel

%description
pyip is a Python package offering assembling/disassembling of raw ip packet, 
including ip, udp, and icmp. Also it includes 2 utilities based on raw ip,
traceroute and ping.

The primary motivation for this project is to fill the blank in Python, i.e.,
handling raw ip packet. Meanwhile, the utility 'traceroute' is intended to be
port of Unix 'traceroute' to Windows platform, as Windows' tracert has only
very limited command line options compared with Unix 'traceroute'.

%prep
%setup -q

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
 
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE
%{python_sitelib}/*

%changelog
* Mon Jun 14 2010 Nathaniel McCallum <nathaniel@natemccallum.com> - 0.7-2
- Add name and version macro to the source url
- Re-download source so that the sums match

* Sat Jun 12 2010 Nathaniel McCallum <nathaniel@natemccallum.com> - 0.7-1
- Initial build
