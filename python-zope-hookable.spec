%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}

%global modname zope.hookable
%global version 3.4.1
%global release 1

Name:           python-zope-hookable
Version:        %{version}
Release:        %{release}%{?dist}
Summary:        Hookable object support

Group:          Development/Libraries
License:        Zope Public License
URL:            http://pypi.python.org/pypi/%{modname}
Source0:        http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel
BuildRequires:  python-setuptools
Requires:       python-zope-testing

%description
Support the efficient creation of hookable objects, which are callable objects
that are meant to be replaced by other callables, at least optionally.

The idea is you create a function that does some default thing and make it
hookable. Later, someone can modify what it does by calling its sethook method
and changing its implementation.  All users of the function, including those
that imported it, will see the change.

%prep
%setup -q -n %{modname}-%{version}
rm -rf src/%{modname}.egg-info

%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT

# Remove strange source code dropping
rm -f $RPM_BUILD_ROOT/%{python_sitearch}/zope/hookable/_zope_hookable.c
 
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitearch}/zope/hookable
%{python_sitearch}/%{modname}-%{version}-*.egg-info
%{python_sitearch}/%{modname}-%{version}-*.pth

%changelog
* Sat Jun 19 2010 Nathaniel McCallum <nathaniel@natemccallum.com> - 3.4.1-1
- Initial package

