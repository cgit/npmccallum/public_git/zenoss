%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

%global modname zope.dublincore
%global version 3.6.3
%global release 1

Name:           python-zope-dublincore
Version:        %{version}
Release:        %{release}%{?dist}
Summary:        Dublin Core support for Zope-based web applications

Group:          Development/Libraries
License:        Zope Public License
URL:            http://pypi.python.org/pypi/%{modname}
Source0:        http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel
BuildRequires:  python-setuptools
Requires:       pytz
Requires:       python-zope-component
Requires:       python-zope-datetime
Requires:       python-zope-interface
Requires:       python-zope-lifecycleevent
Requires:       python-zope-location
Requires:       python-zope-schema
Requires:       python-zope-security

%description
zope.dublincore provides a Dublin Core support for Zope-based web
applications. This includes:
  * an IZopeDublinCore interface definition that can be implemented
    by objects directly or via an adapter to support DublinCore metadata
  * an IZopeDublinCore adapter for annotatable objects (objects providing
    IAnnotatable from zope.annotation)
  * a partial adapter for objects that already implement some of the
    IZopeDublinCore API
  * a "Metadata" browser page (which by default appears in the ZMI)
  * subscribers to various object lifecycle events that automatically set
    the created and modified date and some other metadata

%prep
%setup -q -n %{modname}-%{version}
rm -rf src/%{modname}.egg-info

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
 
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/zope/dublincore
%{python_sitelib}/%{modname}-%{version}-*.egg-info
%{python_sitelib}/%{modname}-%{version}-*.pth

%changelog
* Sat Jun 19 2010 Nathaniel McCallum <nathaniel@natemccallum.com> - 3.6.3-1
- Initial package
