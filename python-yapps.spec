%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

%global upstrname yapps
%global versmajor 2.1.1
%global versminor 17.1

Name:           python-yapps
Version:        %{versmajor}_%{versminor}
Release:        1%{?dist}
Summary:        Yapps is an easy to use parser generator that generates Python code

Group:          Development/Languages
License:        MIT
URL:            http://theory.stanford.edu/~amitp/%{upstrname}
Source0:        http://theory.stanford.edu/~amitp/yapps/%{upstrname}2-%{versmajor}-%{versminor}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel

%description
Yapps (Yet Another Python Parser System) is an easy
to use parser generator that is written in Python
and generates Python code. Yapps is simple, is easy
to use, and produces human-readable parsers. It is
not the fastest, most powerful, or most flexible
parser. Yapps is designed to be used when regular
expressions are not enough and other parser systems
are too much: situations where you may write your
own recursive descent parser.

%prep
%setup -q -n %{upstrname}2-%{versmajor}-%{versminor}

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
 
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc LICENSE
%{python_sitelib}/*

%changelog
* Fri Jun 18 2010 Nathaniel McCallum - 2.1.1_17.1-1
- Initial package

