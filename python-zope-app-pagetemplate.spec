%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

%global modname zope.app.pagetemplate
%global version 3.11.0
%global release 1

Name:           python-zope-app-pagetemplate
Version:        %{version}
Release:        %{release}%{?dist}
Summary:        PageTemplate integration for Zope 3

Group:          Development/Libraries
License:        Zope Public License
URL:            http://pypi.python.org/pypi/%{modname}
Source0:        http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel
BuildRequires:  python-setuptools
Requires:       python-zope-interface
Requires:       python-zope-browserpage >= 3.12.0
Requires:       python-zope-component
Requires:       python-zope-configuration
Requires:       python-zope-dublincore
Requires:       python-zope-i18nmessageid
Requires:       python-zope-interface
Requires:       python-zope-pagetemplate >= 3.5.0
Requires:       python-zope-publisher
Requires:       python-zope-schema
Requires:       python-zope-security
Requires:       python-zope-size
Requires:       python-zope-tales
Requires:       python-zope-traversing

%description
The zope.app.pagetemplate package integrates the Page Template
templating system (zope.pagetemplate) into the Zope 3 application
server.  In particular, it provides:
* a TALES engine implementation that uses Zope's security system for
  checking access,
* a ViewPageTemplateFile class that can be put on a browser page
  class as an attribute and will function as a callable method whose
  result is the rendered template, e.g.:
    from zope.publisher import BrowserPage
    from zope.browserpage import ViewPageTemplateFile

    class HelloWorldPage(BrowserPage):
        __call__ = ViewPageTemplateFile('helloworld.pt')
* TALES namespace adapters for easy access to DublinCore metadata
  (e.g. obj/zope:title) and URL quoting
  (e.g. obj/@@absolute_url/url:quote).

%prep
%setup -q -n %{modname}-%{version}
rm -rf src/%{modname}.egg-info

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
 
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/zope/app/pagetemplate
%{python_sitelib}/%{modname}-%{version}-*.egg-info
%{python_sitelib}/%{modname}-%{version}-*.pth

%changelog
* Sat Jun 19 2010 Nathaniel McCallum <nathaniel@natemccallum.com> - 3.11.0-1
- Initial package
