%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

%global modname zope.copy
%global version 3.5.0
%global release 1

Name:           python-zope-copy
Version:        %{version}
Release:        %{release}%{?dist}
Summary:        Pluggable object copying mechanism

Group:          Development/Libraries
License:        Zope Public License
URL:            http://pypi.python.org/pypi/%{modname}
Source0:        http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel
BuildRequires:  python-setuptools
Requires:       python-zope-interface

%description
This package provides a pluggable way to copy persistent objects. It was once
extracted from the zc.copy package to contain much less dependencies. In fact,
we only depend on zope.interface to provide pluggability.

%prep
%setup -q -n %{modname}-%{version}
rm -rf src/%{modname}.egg-info

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
 
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt LICENSE.txt
%{python_sitelib}/zope/copy
%{python_sitelib}/%{modname}-%{version}-*.egg-info
%{python_sitelib}/%{modname}-%{version}-*.pth

%changelog
* Sat Jun 19 2010 Nathaniel McCallum <nathaniel@natemccallum.com> - 3.5.0-1
- Initial package
