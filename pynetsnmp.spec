%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

Name:           pynetsnmp
Version:        0.28.14
Release:        3%{?dist}
Summary:        Python ctypes bindings for NET-SNMP with Twisted integration

Group:          Development/Languages
License:        GPLv2
URL:            http://www.zenoss.com
Source0:        http://dev.zenoss.com/trac/export/20469/trunk/inst/externallibs/%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel
BuildRequires:  net-snmp-devel

Requires:       net-snmp-libs

%description
Python ctypes bindings for NET-SNMP with Twisted integration
pynetsnmp is a set of Python ctypes binding for NET-SNMP, an
implementation of the Simple Network Management Protocol (SNMP).

pynetsnmp is a replacement for the various Python bindings provided by
PySNMP* implementations.  It also implements a glue with the Python
Twisted Matrix networking framework which replaces the TwistedSNMP
implementation.

%package        twisted
Summary:        Twisted integration for %{name}
Group:          Development/Languages
Requires:       %{name} = %{version}-%{release}
Requires:       python-twisted-core

%description    twisted
The %{name}-twisted package contains the twisted integration for %{name}

%prep
%setup -q

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
 
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc COPYRIGHT.txt LICENSE.txt
%dir %{python_sitelib}/pynetsnmp
%{python_sitelib}/pynetsnmp-*.egg-info
%{python_sitelib}/pynetsnmp/CONSTANTS.py*
%{python_sitelib}/pynetsnmp/SnmpSession.py*
%{python_sitelib}/pynetsnmp/__init__.py*
%{python_sitelib}/pynetsnmp/genconstants.py*
%{python_sitelib}/pynetsnmp/netsnmp.py*
%{python_sitelib}/pynetsnmp/version.py*

%files twisted
%defattr(-,root,root,-)
%doc COPYRIGHT.txt LICENSE.txt
%{python_sitelib}/pynetsnmp/tableretriever.py*
%{python_sitelib}/pynetsnmp/twistedsnmp.py*


%changelog
* Mon Jun 14 2010 Nathaniel McCallum <nathaniel@natemccallum.com> - 0.28.14-3
- Fixed directory ownership
- Included COPYRIGHT.txt and LICENSE.txt in subpackage

* Mon Jun 14 2010 Nathaniel McCallum <nathaniel@natemccallum.com> - 0.28.14-2
- Added twisted subpackage
- Added explicit dep on net-snmp-libs

* Sat Jun 12 2010 Nathaniel McCallum <nathaniel@natemccallum.com> - 0.28.14-1
- Initial build
