%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

Name:           python-snpp
Version:        1.1.2
Release:        2%{?dist}
Summary:        Simple Network Paging Protocol

Group:          Development/Libraries
License:        GPLv2+
URL:            http://launchpad.net/python-snpp
Source0:        http://launchpad.net/%{name}/trunk/%{version}/+download/%{name}-%{version}.tar.gz
Source1:        COPYING.python-snpp
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel
BuildRequires:  python-setuptools

%description
Libraries implementing RFC 1861 - Simple Network Paging Protocol

%prep
%setup -q
rm -rf python_snpp.egg-info
cp %{SOURCE1} ./COPYING

%build
sed -i 's/from ez_setup import use_setuptools//' setup.py
sed -i 's/use_setuptools()//' setup.py
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
 
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README COPYING
%{python_sitelib}/Pager.py*
%{python_sitelib}/snpplib.py*
%{python_sitelib}/python_snpp*.egg-info

%changelog
* Sat Jun 19 2010 Nathaniel McCallum - 1.1.2-2
- Fix egg-info issue
- Add python-setuptools as build requires
- Use more specific files section
- Include COPYING file from upstream bzr
- Fix group and summary

* Fri Jun 18 2010 Nathaniel McCallum - 1.1.2-1
- Initial package

