%global username %{name}
%global spooldir %{_localstatedir}/spool/%{name}
%global piddir   %{_localstatedir}/run/%{name}

Name:           sendpage
Version:        1.000003
Release:        1%{?dist}
Summary:        Dial a modem and send/receive pages via SNPP/TAP

Group:          Applications/Communications
License:        GPLv2+
URL:            http://www.sendpage.org
Source0:        http://www.sendpage.org/download/%{name}-%{version}.tar.gz
Source1:        sendpage.init
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  perl(Device::SerialPort)
BuildRequires:  perl(Mail::Send)
BuildRequires:  perl(Test::More)
BuildRequires:  perl(Net::SNPP)
BuildRequires:  perl(DBI)

BuildArch:      noarch

%description
To send alphanumeric pages to a pager, if an email gateway is unavailable
or undesirable, software is needed to control a modem which will dial a
"Paging Central", and deliver the pages using an ASCII delivery system
known as "TAP".

Sendpage implements all aspects of this type of software, including an
SNPP client, an SNPP server, a queuing engine, a modem control engine, a
TAP communication system, and an email notification system.

%prep
%setup -q

# Fix references to invalid perl
sed -i 's|/usr/local/bin/perl|%{__perl}|' `find -type f`

# Fix improper pidfile location
sed -i 's|/var/spool/sendpage/sendpage|/var/run/sendpage/sendpage|' sendpage
sed -i 's|/tmp/var/spool/sendpage/sendpage|/var/run/sendpage/sendpage|' sendpage.cf

%build
%{__perl} Makefile.PL PREFIX=%{_prefix} INSTALLSITELIB=%{perl_vendorlib}
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT

# Do install
make install DESTDIR=$RPM_BUILD_ROOT

# Remove files we don't care about
rm -rf $RPM_BUILD_ROOT/%{perl_vendorlib}/auto
rm -rf $RPM_BUILD_ROOT/%{perl_archlib}/perllocal.pod

# Fix perms
%{_fixperms} $RPM_BUILD_ROOT/%{_bindir}/*
%{__chmod} 644 $RPM_BUILD_ROOT/%{perl_vendorlib}/Sendpage/*.pm

# Install config
%{__mkdir_p} -m 755 $RPM_BUILD_ROOT/%{_sysconfdir}
%{__install} -m 644 email2page.conf sendpage.cf snpp.conf $RPM_BUILD_ROOT/%{_sysconfdir}

# Install init
%{__mkdir_p} -m 755 $RPM_BUILD_ROOT/%{_initddir}
%{__install} -m 755 %{SOURCE1} $RPM_BUILD_ROOT/%{_initddir}/sendpage

# Make spool and piddir
%{__mkdir_p} -m 755 $RPM_BUILD_ROOT/%{spooldir}
%{__mkdir_p} -m 755 $RPM_BUILD_ROOT/%{piddir}

%clean
rm -rf $RPM_BUILD_ROOT

%pre
getent passwd %{username} >/dev/null || \
    useradd -r -M -d %{spooldir} -s /sbin/nologin -c %{name} %{username}
exit 0

%preun
if [ "$1" = "0" ] ; then
    /sbin/service %{name} stop >/dev/null 2>&1
    /sbin/chkconfig --del %{name}
fi

%post
if [ "$1" = "1" ] ; then
    /sbin/chkconfig --add %{name}
fi

%files
%defattr(-,root,root,-)
%doc LICENSE README FEATURES TODO docs
%config(noreplace) %{_sysconfdir}/*.cf
%config(noreplace) %{_sysconfdir}/*.conf
%attr(-,%{username},root) %{spooldir}
%attr(-,%{username},root) %{piddir}
%{perl_vendorlib}/Sendpage
%{_initddir}/sendpage
%{_mandir}/man1/*
%{_mandir}/man3/*
%{_bindir}/*

%changelog
* Fri Jun 18 2010 Nathaniel McCallum - 1.000003-1
- Initial package

