%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

%global modname zope.testing
%global version 3.9.5
%global release 1

Name:           python-zope-testing
Version:        %{version}
Release:        %{release}%{?dist}
Summary:        Zope Testing Framework
Group:          Development/Libraries
License:        ZPLv2.1
URL:            http://pypi.python.org/pypi/%{modname}
Source0:        http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python-devel
BuildRequires:  python-setuptools
Requires:       python-zope-filesystem

%description
This package provides a number of testing frameworks. It includes a
flexible test runner, and supports both doctest and unittest.

%prep
%setup -q -n %{modname}-%{version}

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
 
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/zope/testing
%{python_sitelib}/%{modname}-%{version}-*.egg-info
%{python_sitelib}/%{modname}-%{version}-*.pth
%{_bindir}/zope-testrunner

%changelog
* Thu Jun 24 2010 Nathaniel McCallum <nathaniel@natemccallum.com> - 3.9.5-1
- Upgrade to 3.9.5

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.7.3-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Jun 15 2009 Conrad Meyer <konrad@tylerc.org> - 3.7.3-3
- Actually fix file conflict with python-zope-filesystem.

* Sat Jun 13 2009 Conrad Meyer <konrad@tylerc.org> - 3.7.3-2
- Fix file conflict with python-zope-filesystem.

* Wed Apr 22 2009 Conrad Meyer <konrad@tylerc.org> - 3.7.3-1
- Bump to 3.7.3.

* Sun Dec 14 2008 Conrad Meyer <konrad@tylerc.org> - 3.7.1-1
- Initial package.
