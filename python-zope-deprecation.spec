%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

%global modname zope.deprecation
%global version 3.4.0
%global release 1

Name:           python-zope-deprecation
Version:        %{version}
Release:        %{release}%{?dist}
Summary:        Module to deprecate python modules, classes, functions, methods and properties

Group:          Development/Libraries
License:        Zope Public License
URL:            http://pypi.python.org/pypi/%{modname}
Source0:        http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel
BuildRequires:  python-setuptools
BuildRequires:  python-zope-testing

%description
Solves the hardest part of the python development process which is to ensure
backward-compatibility and correctly mark deprecated modules, classes,
functions, methods and properties. This package provides a simple function
called 'deprecated(names, reason)' to deprecate the previously mentioned
Python objects.

%prep
%setup -q -n %{modname}-%{version}
rm -rf src/%{modname}.egg-info

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
 
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README.txt
%{python_sitelib}/zope/deprecation
%{python_sitelib}/%{modname}-%{version}-*.egg-info
%{python_sitelib}/%{modname}-%{version}-*.pth

%changelog
* Sat Jun 19 2010 Nathaniel McCallum <nathaniel@natemccallum.com> - 3.4.0-1
- Initial package
