%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

%global modname zope.app.container
%global version 3.8.2
%global release 1

Name:           python-zope-app-container
Version:        %{version}
Release:        %{release}%{?dist}
Summary:        Zope Application Container

Group:          Development/Libraries
License:        Zope Public License
URL:            http://pypi.python.org/pypi/%{modname}
Source0:        http://pypi.python.org/packages/source/z/%{modname}/%{modname}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

BuildRequires:  python-devel
BuildRequires:  python-setuptools
Requires:       python-zope-browser
Requires:       python-zope-component
Requires:       python-zope-container
Requires:       python-zope-copypastemove
Requires:       python-zope-dublincore
Requires:       python-zope-event
Requires:       python-zope-exceptions
Requires:       python-zope-i18n
Requires:       python-zope-i18nmessageid
Requires:       python-zope-interface
Requires:       python-zope-lifecycleevent
Requires:       python-zope-location
Requires:       python-zope-publisher >= 3.12
Requires:       python-zope-schema
Requires:       python-zope-security
Requires:       python-zope-size
Requires:       python-zope-traversing
Requires:       python-zope-app-publisher

%description
This package define interfaces of container components, and provides sample
container implementations such as a BTreeContainer and OrderedContainer.

%prep
%setup -q -n %{modname}-%{version}
rm -rf src/%{modname}.egg-info

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
 
%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/zope/app/container
%{python_sitelib}/%{modname}-%{version}-*.egg-info
%{python_sitelib}/%{modname}-%{version}-*.pth

%changelog
* Sat Jun 19 2010 Nathaniel McCallum <nathaniel@natemccallum.com> - 3.8.2-1
- Initial package
